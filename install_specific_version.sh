# Create system and Postgresql User
sudo useradd -M -r odoo8 --password odoo
sudo su postgres -c "psql -f ./install.sql"

# Create Filestore folder
sudo mkdir ./filestore
sudo chown odoo8 -R  ./filestore
sudo chgrp odoo8 -R  ./filestore
