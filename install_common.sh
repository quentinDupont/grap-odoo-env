# Install Python, postgresql and needed modules
sudo apt-get update -y

sudo apt-get install python-pip -y
sudo apt-get install postgresql -y
sudo apt-get install libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev libssl-dev -y
sudo apt-get install libjpeg8-dev zlib1g-dev -y
# For V10+
sudo apt-get install libtiff5-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk -y

# Install less compiler
sudo apt-get install npm -y
# sudo npm install -g less less-plugin-clean-css

# GRAP needs
sudo apt install libcairo2-dev -y

# Install gitaggregator out of the virtualenv
sudo pip install git-aggregator

# Install wkhtmltopdf 0.12.5 (ref http://www.odoo.com/documentation/10.0/setup/install.html)
sudo wget -P /tmp/ https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb
sudo apt-get install /tmp/wkhtmltox_0.12.5-1.bionic_amd64.deb
sudo rm /tmp/wkhtmltox_0.12.5-1.bionic_amd64.deb

