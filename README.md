# odoo-env-grap


## First time
```shell

# Get Code
git clone https://gitlab.com/grap-rhone-alpes/grap-odoo-env
cd grap-odoo-env

# Install required librairies (first time), create users, etc...
./install.sh

# Launch Odoo
sudo su odoo8 -c "./env/bin/python2 ./src/odoo/odoo.py -c ./odoo.cfg"
```

## Create alias

vim ~/.bashrc
```shell

function run_odoo_8() {
   myCommand2="sudo su odoo8 -c './env/bin/python2 ./src/odoo/odoo.py -c ./odoo.cfg $@'";
   eval $myCommand2;
}

```
source ~/.bashrc

then :

run_odoo_8 -d my_base -u my_module


## Update
```shell

# Update Deployment code
git pull origin 8.0

# Rebuild Odoo Code
./install_code.sh

# Reinstall Python Libs
./install_python_lib.sh
```

## Restore Database
```shell
sudo su postgres -c "psql"

postgres=# create database resto_xxxx_xx_xx owner odoo8 ;
postgres=\q

sudo su postgres -c "pg_restore -d resto_xxxx_xx_xx grap_production.dump --no-owner"
```
