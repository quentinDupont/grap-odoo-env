# Install standard librairies
./env/bin/python2 -m pip install -r ./src/odoo/requirements.txt

# Install custom librairies
./env/bin/python2 -m pip install -r ./requirements.txt
