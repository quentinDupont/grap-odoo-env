import ConfigParser
import yaml
import os

COMMON_FILE_PATH = "./common.odoo.cfg"
CUSTOM_FILE_PATH = "./custom.odoo.cfg"
REPO_FILE_PATH = "./repos.yml"
ODOO_FILE_PATH = "./odoo.cfg"
BASE_ADDON_PATH = os.path.dirname(os.path.realpath(__file__))

parser = ConfigParser.ConfigParser()

# Read Common and custom file
parser.read([COMMON_FILE_PATH, CUSTOM_FILE_PATH])

# Compute Addons path
stream = open(REPO_FILE_PATH, 'r')
data = yaml.load(stream)

addons_path = []
for key in data.keys():
    path = os.path.join(BASE_ADDON_PATH, key)
    if path.split('/')[-1] == 'odoo':
        if os.path.exists(os.path.join(path, 'odoo')):
            # version 10.0 ++
            base_addons_folder = os.path.join(path, 'odoo')
        else:
            # version 9.0 --
            base_addons_folder = os.path.join(path, 'openerp')

        # Add two addons path
        addons_path.append(os.path.join(path, 'addons'))
        addons_path.append(base_addons_folder)
    else:
        addons_path.append(path)

parser.set('options', 'addons_path', ','.join(sorted(addons_path)))

parser.write(open(ODOO_FILE_PATH, 'w'))
