import yaml
import os
import subprocess


REPO_FILE_PATH = "./repos.yml"
DEBUG_LEVEL = False
BASE_ADDON_PATH = os.path.dirname(os.path.realpath(__file__))

# Compute Addons path
stream = open(REPO_FILE_PATH, 'r')
data = yaml.load(stream)

addons_path_list = []
for key in data.keys():
    path = os.path.join(BASE_ADDON_PATH, key)
    addons_path_list.append(path)

for addons_path in sorted(addons_path_list):
    res = subprocess.check_output(['git', 'status'], cwd=addons_path)
    if "la copie de travail est propre" not in res or DEBUG_LEVEL:
        print "".rjust(120, "=")
        print addons_path
        print "".rjust(120, "=")
        print res
